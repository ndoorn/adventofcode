use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let all_lines: Vec<String> = read_lines("./d06.input.txt")
        .unwrap()
        .map(|item| item.unwrap())
        .collect();

    let input = all_lines.first().unwrap();

    let mut fishes = [0usize; 9];

    input
        .split(',')
        .map(|i| i.parse::<usize>().unwrap())
        .for_each(|i| fishes[i as usize] += 1);

    println!("input: {}", input);
    println!("fishes: {:?}", fishes);

    // let mut start_idx = 0;

    for day in 1..257 {
        let zero_fishes = fishes[0];

        // start_idx += 1;
        // start_idx %= 8;
        fishes.rotate_left(1);
        // fishes[start_idx + 6 % 8] += zero_fishes;

        let mut total = 0;
        for fish in fishes {
            total += fish;
        }

        // println!("fish zero is {}", zero_fishes);
        println!("day is {:03} | total {}", day, total);
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

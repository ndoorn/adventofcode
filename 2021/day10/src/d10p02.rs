pub mod d10p2 {
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day10/d10.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut scores : Vec<usize> = vec![];
        for line in all_lines {
            if let Some(v) = process(&line) {
                println!("found a repair value of {}", v);
                scores.push(v);
            }
        }

        // Sort the scores
        scores.sort_unstable();

        // Take the middle result, asserting that the length of 'scores' is always off.
        let middle_result = scores[scores.len() / 2];

        // Print the answer :)
        println!("the answer is {}", middle_result);
    }

    pub fn process(line: &str) -> Option<usize> {
        let mut queue: Vec<char> = vec![];

        let opening = vec!['[', '<', '{', '('];
        let closing = vec![']', '>', '}', ')'];

        for c in line.chars() {
            // println!("-------------");
            // println!("{} ", c);

            if opening.contains(&c) {
                queue.push(c);
                continue;
            }

            let opening_bracket = queue.pop().unwrap();

            let closing_idx = closing.iter().position(|x| *x == c).unwrap();
            let opening_idx = opening.iter().position(|x| *x == opening_bracket).unwrap();

            if closing_idx == opening_idx {
                continue;
            }

            println!("found a irregularity, {} {}", c, opening[opening_idx]);
            return None;
        }
        println!();

        let mut line_score: usize = 0;
        loop {
            line_score *= 5;
            let c = queue.pop().unwrap();
            match c {
                '(' => line_score += 1,
                '[' => line_score += 2,
                '{' => line_score += 3,
                '<' => line_score += 4,
                _ => (),
            }

            if queue.is_empty() {
                break
            }
        }
        Some(line_score)
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
        where
            P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

pub mod d10p1 {
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day10/d10.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut counters: [usize; 4] = [0; 4];
        for line in all_lines {
            if let Some(c) = process(&line) {
                println!("found {} as invalid char", c);
                match c {
                    ')' => counters[0] += 1,
                    ']' => counters[1] += 1,
                    '}' => counters[2] += 1,
                    '>' => counters[3] += 1,
                    _ => (),
                }
            }
        }

        let mut answer = 0;
        answer += counters[0] * 3;
        answer += counters[1] * 57;
        answer += counters[2] * 1197;
        answer += counters[3] * 25137;
        println!("the answer is {}", answer);
    }

    pub fn process(line: &str) -> Option<char> {
        let mut queue: Vec<char> = vec![];

        let opening = vec!['[', '<', '{', '('];
        let closing = vec![']', '>', '}', ')'];

        for c in line.chars() {
            // println!("-------------");
            // println!("{} ", c);

            if opening.contains(&c) {
                queue.push(c);
                continue;
            }

            let opening_bracket = queue.pop().unwrap();

            let closing_idx = closing.iter().position(|x| *x == c).unwrap();
            let opening_idx = opening.iter().position(|x| *x == opening_bracket).unwrap();

            if closing_idx == opening_idx {
                continue;
            }

            println!("found a irregularity, {} {}", c, opening[opening_idx]);
            return Some(c);
        }
        println!();

        None
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let lines = read_lines("./d03.input.txt").unwrap();
    let lines: Vec<String> = lines
        .filter(|item| item.is_ok())
        .map(|item| item.unwrap())
        .filter(|item| item.len() > 0)
        .collect();

    let bits = lines.get(0).unwrap().len();
    println!("number of bits detected is {}", bits);

    let mut count: Vec<i32> = vec![0; bits];
    println!("count : {:?}", count);

    for line in lines {
        let mut chars = line.chars();

        for (idx, single_char) in chars.enumerate() {
            match single_char {
                '1' => count[idx] += 1,
                _ => count[idx] -= 1,
            }
        }
    }

    // from: [3, -2, 1, 5, -2]
    //   to: [1, 0, 1, 1, 0]

    let gamma = count
        .iter()
        .map(|item| if *item > 0 { 1 } else { 0 })
        .collect::<Vec<i32>>();

    let epsilon = gamma.iter().map(|item| 1 - *item).collect::<Vec<i32>>();

    println!("gamma : {:?}", gamma);
    println!("epsilon : {:?}", epsilon);

    // array (bin) => decimal
    let dec_gamma = to_u32(gamma);
    let dec_epsilon = to_u32(epsilon);
    dbg!(dec_gamma);
    dbg!(dec_epsilon);

    println!(
        "The answer is {} * {} = {}",
        dec_gamma,
        dec_epsilon,
        dec_epsilon * dec_gamma
    );
}

fn to_u32(slice: Vec<i32>) -> u32 {
    slice.iter().fold(0, |acc, &b| acc * 2 + b as u32)
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let lines = read_lines("./d03.input.txt").unwrap();
    let lines: Vec<String> = lines
        .filter(|item| item.is_ok())
        .map(|item| item.unwrap())
        .filter(|item| item.len() > 0)
        .collect();

    let bits = lines.get(0).unwrap().len();
    println!("number of bits detected is {}", bits);

    let mut list_gamma: Vec<String> = lines.to_vec();
    let mut list_epsilon: Vec<String> = lines.to_vec();

    let mut bit_pos = 0;

    while list_gamma.len() > 1 {
        list_gamma = filter_list_by_bit_pos(list_gamma.to_vec(), bits, bit_pos, 1);
        bit_pos += 1;
    }

    let mut bit_pos = 0;
    while list_epsilon.len() > 1 {
        list_epsilon = filter_list_by_bit_pos(list_epsilon.to_vec(), bits, bit_pos, 0);
        bit_pos += 1;
    }

    let dec_gamma = str_to_digit(list_gamma.get(0).unwrap());
    let dec_epsilon = str_to_digit(list_epsilon.get(0).unwrap());

    println!(
        "the answer is {} * {} = {}",
        dec_gamma,
        dec_epsilon,
        dec_epsilon * dec_gamma
    );
}

fn filter_list_by_bit_pos(
    input: Vec<String>,
    bits: usize,
    bit_position: usize,
    on_equal: i32,
) -> Vec<String> {
    let mut count: i32 = 0;

    let mut ones: Vec<String> = Vec::new();
    let mut zeroes: Vec<String> = Vec::new();

    for line in input.into_iter() {
        let chars = line.chars();

        let char_value = chars.enumerate().nth(bit_position).unwrap_or((1, 'x')).1;

        match char_value {
            '1' => {
                count += 1;
                ones.push(line);
            }
            '0' => {
                count -= 1;
                zeroes.push(line);
            }
            _ => (),
        }
    }

    let on_equal_return = if on_equal == 0 { &zeroes } else { &ones };

    if on_equal == 0 {
        if count >= 0 {
            return zeroes;
        } else {
            return ones;
        }
    } else {
        if count < 0 {
            return zeroes;
        } else {
            return ones;
        }
    }
}

fn str_to_digit(input: &String) -> i32 {
    return i32::from_str_radix(input, 2).unwrap();
}

fn to_u32(slice: Vec<i32>) -> u32 {
    slice.iter().fold(0, |acc, &b| acc * 2 + b as u32)
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

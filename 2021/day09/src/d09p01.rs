pub mod d9p1 {
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    pub fn main() {
        // 1704 - your answer is too high.
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day09/d09.test.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut all_numbers: Vec<usize> = vec![];

        for l in &all_lines {
            for c in l.chars() {
                let d = c.to_digit(10).unwrap() as usize;
                all_numbers.push(d);
            }
        }

        let width = all_lines.get(0).unwrap().len();
        let height = all_lines.len();

        println!("puzzle is {} by {}", width, height);

        let mut lowest_points: usize = 0;
        let mut risk_level: usize = 0;
        println!("lowest point at X . Y");
        for x in 0..width {
            for y in 0..height {
                let n = *all_numbers.get(y * width + x).unwrap();

                // left
                let left = if x > 0 {
                    *all_numbers.get(y * width + x - 1).unwrap()
                } else {
                    10
                };

                // top
                let top = if y > 0 {
                    *all_numbers.get((y - 1) * width + x).unwrap()
                } else {
                    10
                };

                // right
                let right = if x < (width - 1) {
                    *all_numbers.get(y * width + x + 1).unwrap()
                } else {
                    10
                };

                // bottom
                let bottom = if y < (height - 1) {
                    *all_numbers.get((y + 1) * width + x).unwrap()
                } else {
                    10
                };

                if n < left && n < right && n < top && n < bottom {
                    println!(
                        "lowest point at {:2} x {:2} | {} | {:2} {:2} {:2} {:2}",
                        x, y, n, left, right, top, bottom
                    );
                    lowest_points += 1;
                    risk_level += n + 1;
                }
            }
        }

        // println!("nums: {:?}", all_numbers);
        println!(
            "I found {} lowest points, risk score is {}",
            lowest_points, risk_level
        );
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
        where
            P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

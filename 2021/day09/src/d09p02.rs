pub mod d9p2 {
    use std::collections::HashSet;
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    struct Board {
        width: usize,
        height: usize,
        values: Vec<usize>,
    }
    impl Board {
        fn new(width: usize, height: usize, values: &[usize]) -> Board {
            Self {
                width,
                height,
                values: values.to_vec(),
            }
        }

        fn get_by_point(&self, point: Point) -> Option<usize> {
            self.get(point.x, point.y)
        }

        fn get(&self, x: isize, y: isize) -> Option<usize> {
            if x < 0 || x >= self.width as isize || y < 0 || y >= self.height as isize {
                None
            } else {
                self.values
                    .get(y as usize * self.width + x as usize)
                    .copied()
            }
        }
    }

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day09/d09.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut all_numbers: Vec<usize> = vec![];

        for l in &all_lines {
            for c in l.chars() {
                let d = c.to_digit(10).unwrap() as usize;
                all_numbers.push(d);
            }
        }

        let width = all_lines.get(0).unwrap().len();
        let height = all_lines.len();

        let board = Board::new(width, height, &all_numbers);

        println!("puzzle is {} by {}", width, height);

        let mut lowest_points: usize = 0;
        let mut basins: Vec<usize> = vec![];
        println!("lowest point at X . Y");
        for x in 0..width as isize {
            for y in 0..height as isize {
                let n = board.get(x, y).unwrap();

                let point = Point::new(x, y);

                let all_higher = point.neighbours().iter().fold(true, |all_higher, i| {
                    if let Some(level) = board.get_by_point(*i) {
                        return all_higher && level > n;
                    }

                    false
                });

                if all_higher {
                    println!("lowest point at {:2} x {:2} | {}", x, y, n);

                    let size = get_basin_size(&board, point);
                    basins.push(size);

                    lowest_points += 1;
                }
            }
        }

        // This only sort, the return value is () (this is called a unit tuple)
        basins.sort_unstable();

        let mut answer = 1;
        for _ in 0..3 {
            let x = basins.pop().unwrap();
            print!("{} ", x);
            answer *= x;
        }
        println!(" - :)");

        println!("I found {} basins, answer is {}", lowest_points, answer);
    }

    fn get_basin_size(board: &Board, point: Point) -> usize {
        let mut processed: HashSet<Point> = HashSet::new();

        processed.insert(point);

        expand(board, &mut processed, point);

        println!("found a basin of size: {}", processed.len());
        println!("----");

        processed.len()
    }

    fn expand(board: &Board, processed: &mut HashSet<Point>, point: Point) {
        for neighbour in point.neighbours() {
            if matches!(board.get_by_point(neighbour), Some(x) if x < 9 && processed.insert(neighbour))
            {
                expand(board, processed, neighbour)
            }
        }
    }

    #[derive(PartialEq, Hash, Eq, Copy, Clone)]
    struct Point {
        x: isize,
        y: isize,
    }
    impl Point {
        pub fn new(x: isize, y: isize) -> Point {
            Point { x, y }
        }

        pub fn neighbours(&self) -> Vec<Self> {
            vec![
                Point::new(self.x - 1, self.y),
                Point::new(self.x + 1, self.y),
                Point::new(self.x, self.y - 1),
                Point::new(self.x, self.y + 1),
            ]
        }
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

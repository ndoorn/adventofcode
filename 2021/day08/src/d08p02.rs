pub mod d8p2 {
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    #[derive(Debug)]
    struct Display {
        top: char,
        top_right: char,
        top_left: char,
        middle: char,
        bottom_right: char,
        bottom_left: char,
        bottom: char,
    }

    impl Display {
        fn letters(&self) -> String {
            let mut output = "".to_owned();

            output.push(self.top);
            output.push(self.top_left);
            output.push(self.top_right);
            output.push(self.middle);
            output.push(self.bottom_left);
            output.push(self.bottom_right);

            output
        }
        fn new() -> Display {
            Display {
                top: ' ',
                top_right: ' ',
                top_left: ' ',
                middle: ' ',
                bottom_right: ' ',
                bottom_left: ' ',
                bottom: ' ',
            }
        }
    }

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day08/d08.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut count = 0;

        // input: bgcad, gcb
        println!("common test: {}", common("bgcad", "gcb"));

        for line in all_lines {
            let mut display: Display = Display::new();

            let mut split_line = line.split('|');
            let first_part = split_line.next().unwrap();
            let second_part = split_line.next().unwrap();

            let mut nums: [&str; 10] = [""; 10];
            let scrambled = first_part.split_ascii_whitespace().collect::<Vec<&str>>();

            let nr_one = scrambled
                .clone()
                .into_iter()
                .filter(|item| item.len() == 2)
                .collect::<Vec<&str>>()[0];
            nums[1] = nr_one;

            let nr_seven = scrambled
                .clone()
                .into_iter()
                .filter(|item| item.len() == 3)
                .collect::<Vec<&str>>()[0];
            nums[7] = nr_seven;

            let nr_four = scrambled
                .clone()
                .into_iter()
                .filter(|item| item.len() == 4)
                .collect::<Vec<&str>>()[0];
            nums[4] = nr_four;

            display.top = diff(nr_seven, nr_one).chars().next().unwrap();

            println!("---");
            println!("processing: {}", line);
            println!("  one {}", nums[1]);
            println!(" four {}", nums[4]);
            println!("seven {}", nums[7]);

            let len_six = scrambled
                .clone()
                .into_iter()
                // .map(|i| *i)
                .filter(|i| i.len() == 6)
                .collect::<Vec<_>>();

            for l_six in len_six.clone() {
                if common(l_six, nr_seven).len() == 2 {
                    nums[6] = l_six;
                    println!("  six {}", nums[6]);
                    display.top_right = sub(nums[7], &common(nums[6], nums[7]))
                        .chars()
                        .next()
                        .unwrap();
                }
            }

            let len_five = scrambled
                .clone()
                .into_iter()
                .filter(|i| i.len() == 5)
                .collect::<Vec<_>>();

            for l_five in len_five.clone() {
                println!("processing size of 5: {}", l_five);
                if common(l_five, nums[7]).len() == 3 {
                    nums[3] = l_five;
                    println!("three: {}", nums[3]);
                    break;
                }
            }

            for l_five in len_five {
                if l_five == nums[3] {
                    continue;
                }

                if common(l_five, nr_seven).contains(display.top_right) {
                    nums[2] = l_five;
                    println!("  two {}", nums[2]);
                    display.bottom_left = sub(nums[2], &common(nums[2], nums[3]))
                        .chars()
                        .next()
                        .unwrap();

                    display.bottom_right = sub(nums[3], &common(nums[2], nums[3]))
                        .chars()
                        .next()
                        .unwrap();
                } else {
                    nums[5] = l_five;
                    println!(" five {}", nums[7]);
                    display.top_left = sub(nums[5], &common(nums[5], nums[3]))
                        .chars()
                        .next()
                        .unwrap();
                }
            }

            for l_six in len_six {
                if l_six == nums[6] {
                    continue;
                }

                if sub(l_six, nums[5]).len() == 1 {
                    nums[9] = l_six
                } else {
                    nums[0] = l_six
                }
            }

            display.middle = diff("abcdefg", nums[0]).chars().next().unwrap();
            display.bottom = diff("abcdefg", &display.letters()).chars().next().unwrap();

            println!("display: {:?}", display);

            let convert: [String; 10] = [
                // 0
                vec![
                    display.top,
                    display.top_right,
                    display.top_left,
                    display.bottom,
                    display.bottom_right,
                    display.bottom_left,
                ]
                .into_iter()
                .collect(),
                // 1
                vec![display.top_right, display.bottom_right]
                    .into_iter()
                    .collect(),
                // 2
                vec![
                    display.top,
                    display.top_right,
                    display.middle,
                    display.bottom_left,
                    display.bottom,
                ]
                .into_iter()
                .collect(),
                // 3
                vec![
                    display.top,
                    display.top_right,
                    display.middle,
                    display.bottom_right,
                    display.bottom,
                ]
                .into_iter()
                .collect(),
                // 4
                vec![
                    display.top_left,
                    display.top_right,
                    display.middle,
                    display.bottom_right,
                ]
                .into_iter()
                .collect(),
                // 5
                vec![
                    display.top,
                    display.top_left,
                    display.middle,
                    display.bottom_right,
                    display.bottom,
                ]
                .into_iter()
                .collect(),
                // 6
                vec![
                    display.top,
                    display.top_left,
                    display.middle,
                    display.bottom_right,
                    display.bottom_left,
                    display.bottom,
                ]
                .into_iter()
                .collect(),
                // 7
                vec![display.top, display.top_right, display.bottom_right]
                    .into_iter()
                    .collect(),
                // 8
                vec![
                    display.top,
                    display.top_right,
                    display.top_left,
                    display.middle,
                    display.bottom_left,
                    display.bottom_right,
                    display.bottom,
                ]
                .into_iter()
                .collect(),
                // 9
                vec![
                    display.top,
                    display.top_right,
                    display.top_left,
                    display.middle,
                    display.bottom_right,
                    display.bottom,
                ]
                .into_iter()
                .collect(),
            ];

            let sorted = convert.clone().map(|i| {
                let mut sorted = i.chars().collect::<Vec<char>>();
                sorted.sort_unstable();
                String::from_iter(sorted)
            });

            let mut line_value: usize = 0;
            for scrambles_value in second_part.split_ascii_whitespace() {
                line_value *= 10;
                line_value += number_by_config(&sorted, scrambles_value);
                println!("line converted from {} to {}", scrambles_value, line_value);
            }
            count += line_value;

            // todo, capture the four numbers on the right side,
            // parse them to a usize and generate a value out of this
            // todo, add the calculated value on top of the global count

            // for element in second_part.split_ascii_whitespace() {
            //     // 1, 4, 7, 8
            //     // 2, 4, 3, 7
            //     match element.len() {
            //         2 => count += 1,
            //         4 => count += 1,
            //         3 => count += 1,
            //         7 => count += 1,
            //         _ => {}
            //     };
            // }
        }

        println!("sum for part two is {}", count);
    }

    fn number_by_config(numbers: &[String; 10], number_as_string: &str) -> usize {
        println!(
            "number_by_config_input: {:?} | {}",
            numbers, number_as_string
        );
        let mut nas = number_as_string.chars().collect::<Vec<char>>();
        nas.sort_unstable();
        let sorted = String::from_iter(nas);

        for (index, element) in numbers.clone().iter().enumerate() {
            // println!("comparing : {:?} against {:?}", element, sorted);
            if element == &sorted {
                return index;
            }
        }

        panic!("invalid thing found here");
    }

    fn diff<'a>(a: &'a str, b: &'a str) -> String {
        let mut diff = "".to_owned();

        for c in a.chars() {
            if !b.contains(c) {
                diff.push(c);
            }
        }

        diff
    }

    fn common<'a>(a: &'a str, b: &'a str) -> String {
        let mut common = "".to_owned();

        for c in a.chars() {
            if b.contains(c) {
                common.push(c);
            }
        }

        common
    }

    fn sub<'a>(origin: &'a str, subtract: &'a str) -> String {
        let mut output = "".to_owned();

        for c in origin.chars() {
            if !subtract.contains(c) {
                output.push(c);
            }
        }

        output
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

mod d08p02;

fn main() {
    d08p02::d8p2::main();
}

// first line of example input:
// acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab
// $$$$$$$                   !!!        %%%%%% @@@@        ##

// # => 1
// @ => 4
// % => 6
// ! => 7
// $ => 8

// len of 2: [ 1 ]
// len of 3: [ 7 ]
// len of 4: [ 4 ]
// len of 5: [ 2, 3, 5 ]
// len of 6: [ 6, 9, 0 ]
// len of 7: [ 8 ] <= useless

// 1 == len(2)
// 2 ==
// 3 == len(5) && full '7' || check '4' for middle and remaining is bottom
// 4 == len(4)
// 5 ==
// 6 == len(6) && 1/2 `1` (assuming you know '1')
// 7 == len(3)
// 8 == len(7)
// 9 ==

// * find '1' => find '6'
// * find '7' => find '3'
// * deduct 2, 3, 5

// * find the unique values (1,4,7,8)
// * top == diff(1,7)

// * find 3 based on 7 (len(5) which fully contains 7)
// [5]: cdfbe gcdfa fbcad
//        5     2     3

// * find 6 based on len(6) and not fully contains 7
// top right == the value of 7 not hit.
// ^-- 2,5 based on top right value
// then we also know the bottom left value (common(2,5) - (1)

// 4 = eafb
// top == d
// right top == a
// right bot == b

//  dd  |
// e  a |
//  ff  |
// g  b |
//  cc  |

// 8

// |
// cdfeb fcadb cdfeb cdbaf

// 1 4 7 (top)
// 6     (top right)
// 3
// 2     (bot right, bot left)
// 5     (top left)

// mid, bot

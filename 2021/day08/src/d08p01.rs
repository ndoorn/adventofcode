pub mod d8p1 {
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day08/d08.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut count = 0;

        for line in all_lines {
            let mut split_line = line.split('|');
            let first_part = split_line.next().unwrap();
            let second_part = split_line.next().unwrap();

            print!("-- ");
            let mut sub_count = 0;
            let mut bools: [bool; 4] = [false; 4];

            for element in first_part.split_ascii_whitespace() {
                // 1, 4, 7, 8
                // 2, 4, 3, 7

                match element.len() {
                    2 => {
                        bools[0] = true;
                        sub_count += 1;
                        print!("[1]");
                    }
                    4 => {
                        bools[1] = true;
                        sub_count += 1;
                        print!("[4]");
                    }
                    3 => {
                        bools[2] = true;
                        sub_count += 1;
                        print!("[3]");
                    }
                    7 => {
                        bools[3] = true;
                        sub_count += 1;
                        print!("[7]");
                    }
                    _ => {
                        print!("[ ]");
                    }
                };
            }
            print!(" || {}", sub_count);

            for b in bools {
                if b {
                    print!("[x]");
                } else {
                    print!("[ ]");
                }
            }

            println!();
            count += sub_count;

            println!("second_part {}", second_part);
        }

        println!("count for part one is {}", count);
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

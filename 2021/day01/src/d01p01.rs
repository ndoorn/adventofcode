use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

pub fn main() {
    let depths: Vec<i32> = read_lines("/home/ndoorn/coding/personal/aoc/2021/day01/d01.input.txt")
        .unwrap()
        .filter_map(|item| item.ok())
        .filter_map(|item| item.parse::<i32>().ok())
        .collect();

    let mut answer: usize = 0;

    for i in 1..depths.len() {
        if depths[i] > depths[i - 1] {
            answer += 1;
        }
    }

    println!("answer is {}", answer);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

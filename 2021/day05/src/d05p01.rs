use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

// Real input has max number around 1000x1000
struct Board {
    width: isize,
    point_count: Vec<usize>,
}

impl Board {
    fn new() -> Self {
        return Board {
            width: 1000,
            point_count: vec![0; 1_000_000],
        };
    }
}

#[derive(Debug)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn new(input: &str) -> Line {
        let mut split = input.split(" -> ");

        let left = Point::new(split.next().unwrap());
        let right = Point::new(split.next().unwrap());

        return Line {
            start: left,
            end: right,
        };
    }

    fn points(&self, allow_diagonal: bool) -> Vec<Point> {
        let dx: isize;
        let dy: isize;

        match self.start.x - self.end.x {
            i if i > 0 => dx = -1,
            i if i < 0 => dx = 1,
            _ => dx = 0,
        }

        match self.start.y - self.end.y {
            i if i > 0 => dy = -1,
            i if i < 0 => dy = 1,
            _ => dy = 0,
        }

        // Do not allow allow_diagonal lines if requested
        if !allow_diagonal && dx != 0 && dy != 0 {
            return vec![];
        }

        let mut points: Vec<Point> = vec![];
        let mut curr_point = self.start;

        while curr_point != self.end {
            points.push(curr_point);
            curr_point.x += dx;
            curr_point.y += dy;
        }

        // Don't forget this one
        points.push(self.end);

        return points;
    }
}

// Copy - make copy of same instance. (This is a lazy variant of clone, less typing)
// Clone - make new separate instance with same values?
// PartialEq - Auto generates the .eq (==) and .ne (!=) operations based on internal members
// #[derive(Debug, Copy, Clone, PartialEq)]
#[derive(Debug, Copy, Clone)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    fn new(input: &str) -> Point {
        let mut split = input.split(',');

        let x = split.next().unwrap().parse::<isize>().unwrap();
        let y = split.next().unwrap().parse::<isize>().unwrap();

        return Point { x, y };
    }
}

/*
 * This can also be implemented by adding the PartialEq in the Device section of the structure
 */
impl PartialEq for Point {
    fn eq(&self, other: &Point) -> bool {
        self.x == other.x && self.y == other.y
    }

    fn ne(&self, other: &Point) -> bool {
        !self.eq(other)
    }
}

fn main() {
    let lines: Vec<Line> = read_lines("./d05.input.txt")
        .unwrap()
        .map(|item| item.unwrap())
        .map(|item| Line::new(&item))
        .collect();

    let first = lines.first().unwrap();
    println!("points {:?}", first.points(false));

    // Fill the board with the found points
    let mut board = Board::new();

    for line in lines {
        let diagonals_allowed = true; // false for part 1, true for part 2
        for point in line.points(diagonals_allowed) {
            let index = board.width * point.y + point.x;
            board.point_count[index as usize] += 1;
        }
    }

    let mut count: usize = 0;
    for board_value in board.point_count {
        if board_value >= 2 {
            count += 1;
        }
    }

    println!("The answer is {}", count);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

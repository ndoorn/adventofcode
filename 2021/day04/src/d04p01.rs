use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

#[derive(Debug, Copy, Clone)]
struct Board {
    items: [usize; 25],
    index: [usize; 100],
    checked: [bool; 25],
}

impl Board {
    fn new() -> Board {
        Board {
            items: [0; 25],
            index: [255; 100],
            checked: [false; 25],
        }
    }

    fn set_checked(board: &mut Board, index: usize) {
        board.checked[index] = true;
    }
}

struct Game {
    board: Vec<Board>,
    balls: Vec<usize>,
}

impl Game {
    fn new() -> Game {
        Game {
            board: vec![],
            balls: vec![],
        }
    }
}

fn main() {
    let mut lines: Vec<String> = read_lines("./d04.input.txt")
        .unwrap()
        .map(|item| item.unwrap())
        .collect();

    let balls: Vec<usize> = lines
        .remove(0)
        .split(',')
        .map(|i| i.parse::<usize>().unwrap())
        .collect();

    println!("the balls are {:?}", balls);

    let mut game: Game = Game::new();

    for ball in balls {
        game.balls.push(ball);
    }

    let mut board: Board = Board::new();
    let mut index: usize = 0;

    println!("board is: {:?}", board.items);
    for line in lines {
        println!("line is : {}", line);

        if line.is_empty() {
            if index > 0 {
                game.board.push(board);
            }
            board = Board::new();
            index = 0;
            continue;
        }

        line.split_whitespace()
            .map(|i| i.parse::<usize>().unwrap())
            .for_each(|i| {
                board.items[index] = i;
                board.index[i] = index;
                index += 1;
            })
    }

    if index > 0 {
        game.board.push(board);
    }

    println!("------");

    println!("{:?}", game.balls);
    for board in &game.board {
        println!("------");
        println!("board: {:?}", board.items);
        println!("index: {:?}", board.index);
    }

    // Preparation is now done. Let us play Bingo.

    for ball in game.balls.to_vec() {
        println!("Crossing off ball {}", ball);
        cross_off_ball(&mut game, &ball);

        for board in &mut game.board {
            if is_solved(board) {
                println!("We found a solved board {:?}", board.items);
                println!("The last ball drawn is {}", ball);

                let sum = sum_unmarked_numbers(board);
                println!("sum is {}", sum);
                println!("answer is {} * {} = {}", ball, sum, ball * sum);
                return;
            }
        }
    }
}

fn sum_unmarked_numbers(board: &Board) -> usize {
    let mut sum: usize = 0;
    for index in 0..25 {
        if !board.checked[index] {
            sum += board.items[index];
        }
    }

    return sum;
}

fn cross_off_ball(game: &mut Game, ball: &usize) {
    for board in &mut game.board {
        if board.index[*ball] == 255 {
            continue;
        }

        let index = board.index[*ball];
        Board::set_checked(board, index);
    }
}

fn is_solved(board: &mut Board) -> bool {
    let winning_indices: [Vec<usize>; 10] = [
        // All horizontal bingos
        vec![0, 1, 2, 3, 4],
        vec![5, 6, 7, 8, 9],
        vec![10, 11, 12, 13, 14],
        vec![15, 16, 17, 18, 19],
        vec![20, 21, 22, 23, 24],
        // All vertical bingos
        vec![0, 5, 10, 15, 20],
        vec![1, 6, 11, 16, 21],
        vec![2, 7, 12, 17, 22],
        vec![3, 8, 13, 18, 23],
        vec![4, 9, 14, 19, 24],
    ];

    for win_option in winning_indices {
        let mut all_checked = true;

        for index in win_option {
            if !board.checked[index] {
                all_checked = false;
                break;
            }
        }

        if all_checked {
            return true;
        }
    }

    return false;
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

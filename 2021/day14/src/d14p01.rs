pub mod d14p1 {
    use std::collections::HashMap;
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    #[derive(Debug)]
    struct Recipe {
        input: String,
        output: String,
    }

    impl Recipe {
        fn from(input: &String) -> Self {
            let mut split = input.split(" -> ");

            let input = split.next().unwrap();
            let output = split.next().unwrap();

            return Recipe {
                input: String::from(input),
                output: String::from(output),
            }
        }
    }

    #[derive(Debug)]
    struct Problem {
        recipes: Vec<Recipe>,
        formula: String,
    }
    impl Problem {
        fn new(formula: String) -> Self {
            Self {
                recipes: vec![],
                formula,
            }
        }
    }

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day14/d14.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let problem = parse_input(all_lines);

        println!("problem: {:?}", problem);

        let mut formula = problem.formula.clone();
        for _ in 0..10 {
            formula = iteration(&formula, &problem);
            // println!("formula is now: {}", formula);
        }
        let (min, max) = min_max_string(&formula);
        println!("min: {}", min);
        println!("max: {}", max);
        println!("val: {}", max - min);
    }

    fn min_max_string(input: &str) -> (usize, usize) {
        let mut min: usize = 99999;
        let mut max: usize = 0;

        let mut counter: HashMap<char, usize> = HashMap::new();

        for x in input.chars() {
            let option = counter.get(&x);

            if option.is_none() {
                counter.insert(x, 1);
                continue;
            }

            let current_count = option.unwrap();
            counter.insert(x, current_count + 1);
        }

        for value in counter.values() {
            if *value < min {
                min = *value;
            }

            if *value > max {
                max = *value;
            }
        }

        (min, max)
    }

    fn iteration(input: &str, problem: &Problem) -> String {
        let modification = generate(input, problem);
        let output = merge(input, &modification);
        output
    }

    fn generate(input: &str, problem: &Problem) -> String {
        let mut output: String = String::new();

        let length = input.len();
        for (idx, c) in input.chars().enumerate() {
            if idx == length - 1{
                continue
            }

            let first = c;
            let second = input.chars().nth(idx + 1).unwrap();

            let mut combined = String::new();
            combined.push(first);
            combined.push(second);

            let recipe = problem.recipes.iter()
                .filter(|i| i.input == combined)
                .nth(0)
                .unwrap();

            output.push(recipe.output.chars().nth(0).unwrap());
        }

        output
    }

    fn merge(input: &str, modification: &str) -> String {
        let mut output = String::new();

        let last_index = modification.len();

        let mut input_chars = input.chars();
        let mut mod_chars = modification.chars();

        for idx in 0..last_index {
            let x = input_chars.nth(0).unwrap();
            output.push(x);
            let y = mod_chars.nth(0).unwrap();
            output.push(y);
        }
        output.push(input_chars.nth(0).unwrap());

        output
    }

    fn parse_input(input: Vec<String>) -> Problem {
        let formula = input.iter().nth(0).unwrap();

        let mut problem: Problem = Problem::new(formula.clone());

        input.iter().skip(2)
            .map(|x| Recipe::from(x))
            .for_each(|x| problem.recipes.push(x));

        problem
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

pub mod d14p2 {
    use std::borrow::Borrow;
    use std::collections::HashMap;
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    #[derive(Debug)]
    struct Recipe {
        input: String,
        output: String,
        pair: [String; 2],
    }

    impl Recipe {
        fn from(input: &String) -> Self {
            let mut split = input.split(" -> ");

            let input = split.next().unwrap();
            let output = split.next().unwrap();

            let mut chars = input.chars();

            let mut first = String::from(chars.next().unwrap());
            let mut second = String::from(output.chars().next().unwrap());

            first.push(output.chars().next().unwrap());
            second.push(chars.next().unwrap());

            return Recipe {
                input: String::from(input),
                output: String::from(output),
                pair: [first, second],
            }
        }
    }

    #[derive(Debug)]
    struct Problem {
        recipes: Vec<Recipe>,
        formula: String,
    }
    impl Problem {
        fn new(formula: String) -> Self {
            Self {
                recipes: vec![],
                formula,
            }
        }
    }

    // 2192039469603 too low, reason: ran the example input and bug
    // 2188189693529 too low, reason: ran the example input
    pub fn main() {
        let all_lines: Vec<String> =
            // read_lines("/home/ndoorn/coding/personal/aoc/2021/day14/d14.input.txt")
            read_lines("/storage/code-links/rust/advent_of_code/2021/day14/d14.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let problem = parse_input(all_lines);

        println!("problem: {:?}", problem);

        let mut map: HashMap<String, usize> = HashMap::new();
        for recipe in problem.recipes.iter() {
            map.insert(String::from(&recipe.input), 0);
        }

        map = fill_map_with_start_value(map, &problem.formula);

        for _ in 0..40 {
            println!();
            print_map(map.borrow());
            map = iteration2(map, &problem);
            min_max_string(&map);
            print_map(map.borrow());
        }

        let (min, max) = min_max_string(&map);
        println!("min: {}", min);
        println!("max: {}", max);
        println!("val: {}", max - min);
    }

    fn fill_map_with_start_value(mut map: HashMap<String, usize>, start_value: &str) -> HashMap<String, usize> {
        let length = start_value.len();

        for idx in 1..length {
            let mut chars = start_value.chars();
            let first = chars.nth(idx - 1).unwrap();
            let second = chars.next().unwrap();

            println!("chars is {}{}", first, second);
            let key = format!("{}{}", first, second);
            let old_value = map.get(&key).unwrap();

            map.insert(key, old_value + 1);
        }

        return map;
    }
    
    fn print_map(map: &HashMap<String, usize>) {
        println!();
        for (k, v) in map.iter() {
            println!("<{}> {}", k, v);
        }
    }

    fn iteration2(map: HashMap<String, usize>, problem: &Problem) -> HashMap<String, usize> {
        let iter_map = map.clone();
        let mut work_map: HashMap<String, usize> = HashMap::new();

        for (key, value) in iter_map.iter() {
            let recipe = get_from_vec(&problem.recipes, &key);
            if recipe.is_none() {
                panic!("Impossibru");
            }

            if *value == 0 {
                continue;
            }

            let pair = recipe.unwrap().pair.clone();

            println!("Expanding {} to {} and {}, {}", key, &pair.get(0).unwrap(), &pair.get(1).unwrap(), value);

            // decrease original formula
            // let z = iter_map.get(key).unwrap().clone();
            // work_map.insert(key.clone(), z - value);

            // increase new pair of formulas
            let mut a = 0;
            if work_map.contains_key(&pair[0]) {
                 a = work_map.get(&pair[0]).unwrap().clone();
            }
            let mut b = 0;
            if work_map.contains_key(&pair[1]) {
                 b = work_map.get(&pair[1]).unwrap().clone();
            }

            work_map.insert(pair[0].clone(), a + value);
            work_map.insert(pair[1].clone(), b + value);

            println!("Updated {} from {} to {}", &pair[0], a, a + value);
            println!("Updated {} from {} to {}", &pair[1], b, b + value);
        }

        return work_map;
    }

    fn get_from_vec<'a>(vec: &'a Vec<Recipe>, key: &str) -> Option<&'a Recipe> {
        for x in vec {
            if x.input == key {
                return Some(x);
            }
        }

        None
    }

    fn parse_input(input: Vec<String>) -> Problem {
        let formula = input.iter().nth(0).unwrap();

        let mut problem: Problem = Problem::new(formula.clone());

        input.iter().skip(2)
            .map(|x| Recipe::from(x))
            .for_each(|x| problem.recipes.push(x));

        problem
    }

    fn min_max_string(input: &HashMap<String, usize>) -> (usize, usize) {
        let mut min: usize = usize::MAX;
        let mut max: usize = usize::MIN;

        let mut counter: HashMap<char, usize> = HashMap::new();

        for (key, value) in input.iter() {
            for c in key.chars() {
                let option = counter.get(&c);

                if option.is_none() {
                    counter.insert(c, value.clone());
                    continue;
                }

                let current_count = option.unwrap();
                counter.insert(c, current_count + value);
            }
        }

        let mut final_map : HashMap<char, usize> = HashMap::new();
        for (ch, amount) in counter.iter() {
            let offset = amount % 2;

            final_map.insert(ch.clone(), amount / 2 + offset);
        }

        for value in final_map.values() {
            if *value < min {
                min = *value;
            }

            if *value > max {
                max = *value;
            }
        }

        println!("dbg: {:?}", final_map);

        (min, max)
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
        where
            P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

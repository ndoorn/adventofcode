pub mod d13p1 {

    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    #[derive(Debug, Clone)]
    struct Problem {
        paper: Paper,
        folds: Vec<Fold>,
    }

    impl Problem {
        fn new() -> Self {
            Self {
                paper: Paper::new(),
                folds: vec![],
            }
        }

        fn contains(&self, x: usize, y: usize) -> bool {
            return self.clone().paper.contains(x, y);
        }
    }

    #[derive(Debug, Clone, Copy)]
    struct Point {
        x: usize,
        y: usize,
    }

    impl Point {
        fn new(x: usize, y: usize) -> Self {
            Self { x, y }
        }
    }

    #[derive(Debug, Clone)]
    struct Paper {
        points: Vec<Point>,
        max_width: usize,
        max_height: usize,
    }

    impl Paper {
        fn new() -> Self {
            Self {
                points: vec![],
                max_width: 0,
                max_height: 0,
            }
        }

        fn contains(self, x: usize, y: usize) -> bool {
            if x > self.max_width || y > self.max_height {
                return false;
            }

            for ele in self.points {
                if ele.x == x && ele.y == y {
                    return true;
                }
            }

            return false;
        }

        fn insert(&mut self, point: Point) -> &Self {
            if self.max_width < point.x {
                self.max_width = point.x
            }

            if self.max_height < point.y {
                self.max_height = point.y
            }

            self.points.push(point);

            self
        }
    }

    #[derive(Debug, Clone, Copy)]
    struct Fold {
        axis: char,
        location: usize,
    }
    impl Fold {
        fn new(axis: char, location: usize) -> Self {
            Self { axis, location }
        }
    }

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day13/d13.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut map = parse_input(all_lines);

        println!("before: {:?}", map);

        let mut frist_fold = true;
        for fold in &map.folds {
            if !frist_fold {
                continue;
            }

            frist_fold = false;
            // println!("---");
            // print(&map);

            let seam = fold.location;

            if fold.axis == 'y' {
                // 0 1 2 3 4 5 |6| 7 8 9 10 11 12

                for y in 0..seam {
                    for x in 0..=map.paper.max_width {
                        let left = map.contains(x, y);
                        let right = map.contains(x, seam * 2 - y);
                        // println!();
                        // println!(
                        //     "checking x: {} and {}, y {}. left {}, right {}",
                        //     x,
                        //     seam * 2 - x,
                        //     y,
                        //     left,
                        //     right
                        // );

                        if right {
                            map.paper.insert(Point::new(x, y));
                        }
                    }
                }
                println!("Lowering max height to {}", seam);
                map.paper.max_height = seam;
            } else {
                // 0 1 2 3 4 5 |6| 7 8 9 10 11 12

                for y in 0..=map.paper.max_height {
                    for x in 0..seam {
                        let left = map.contains(x, y);
                        let right = map.contains(seam * 2 - x, y);

                        if left || right {
                            map.paper.insert(Point::new(x, y));
                        }
                    }
                }

                println!("Lowering max width to {}", seam);
                map.paper.max_width = seam;
            }
        }

        // println!("after: {:?}", map);
        println!(
            "max height is now: {} / {}",
            map.paper.max_height, map.paper.max_width
        );

        print(&map);
    }

    fn print(problem: &Problem) {
        let mut dots: usize = 0;
        for y in 0..=problem.paper.max_height {
            for x in 0..problem.paper.max_width {
                if problem.contains(x, y) {
                    dots += 1;
                    print!("X");
                } else {
                    print!(".");
                }
            }
            println!("dots: {}", dots);
            println!();
        }
    }

    fn parse_input(input: Vec<String>) -> Problem {
        let mut fold_mode = false;

        let mut problem = Problem::new();

        for line in input {
            if line.is_empty() {
                fold_mode = true;
                continue;
            }

            if !fold_mode {
                let mut split = line.split(',');

                let left = split.next().unwrap().parse::<usize>().unwrap();
                let right = split.next().unwrap().parse::<usize>().unwrap();

                let point = Point::new(left, right);

                problem.paper.insert(point);
            } else {
                let split = line.split_at("fold along ".len()).1;
                let mut action = split.split('=');

                let left = action.next().unwrap().chars().next().unwrap();
                let right = action.next().unwrap().parse::<usize>().unwrap();

                problem.folds.push(Fold::new(left, right));
            }
        }

        problem
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

pub mod d11p1 {
    use std::fmt;
    use std::fmt::Formatter;
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    #[derive(Default, Copy, Clone)]
    struct Octopus {
        x: usize,
        y: usize,
        energy: u8,
        flash: bool,
    }

    impl Octopus {
        fn new(x: usize, y: usize) -> Self {
            Self {
                x,
                y,
                energy: 0,
                flash: false,
            }
        }

        fn energy(&self, energy: u8) -> Octopus {
            Self {
                x: self.x,
                y: self.y,
                energy,
                flash: self.flash
            }
        }

        fn flash(&self, flash: bool) -> Octopus {
            Self {
                x: self.x,
                y: self.y,
                energy: self.energy,
                flash
            }
        }
    }

    impl fmt::Display for Octopus {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            write!(f, "{:2}", self.energy)
        }
    }

    impl fmt::Display for Cavern {
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            for y in 0..10 {
                for x in 0..10 {
                    write!(f, "{}", self.octopus.get(y*10 + x).unwrap());
                }
                write!(f, "\n");
            }
            Ok(())
        }
    }


    #[derive(Copy, Clone)]
    struct Cavern {
        flashes: usize,
        octopus: [Octopus; 100],
    }

    impl Cavern {
        fn new() -> Self {
            let mut octopuses: [Octopus; 100] = [Default::default(); 100];

            for x in 0..10 as usize {
                for y in 0..10 as usize {
                    octopuses[y*10 + x] = Octopus::new(x, y);
                }
            }

            Self {
                flashes: 0,
                octopus: octopuses,
            }
        }

        fn get(&self, x: usize, y: usize) -> Octopus {
            return self.octopus[y*10 + x];
        }

        fn set(&mut self, x: usize, y: usize, e: u8, f: bool) {
            self.octopus[y*10 + x] = Octopus {
                x,
                y,
                energy: e,
                flash: f,
            }
        }

        fn neighbours(&self, octopus: &Octopus) -> Vec<Octopus> {
            let mut output: Vec<Octopus> = vec![];

            let x = octopus.x;
            let y = octopus.y;

            for x_delta in -1..=1 as isize {
                let new_x = x as isize + x_delta;

                if new_x < 0 {continue}
                if new_x >= 10 {continue}

                for y_delta in -1..=1 as isize {
                    let new_y = y as isize + y_delta;

                    // This is your own location
                    if x_delta == 0 && y_delta == 0 {continue}

                    if new_y < 0 {continue}
                    if new_y >= 10 {continue}

                    output.push(self.get(new_x as usize, new_y as usize))
                }
            }

            return output;
        }

        /// Runs a single iteration of the AoC problem
        ///
        /// Returns a `true` value is if any of the octopus flashed
        fn step(&mut self) {
            // Part 1: Increment all values by one
            for octo in self.octopus {
                self.set(octo.x, octo.y, octo.energy + 1, octo.flash)
            }

            // Part 2: Flash all 9's
            // repeat until no more flashes
            loop {
                let octopus_flashed = self.flash_octopus();

                if octopus_flashed > 0 {
                    break
                }
            }

            // Part n:
            // - reset all flashed octopuses to '0'
            // - reset the 'flash' variable
            for octo in self.octopus {
                if octo.flash {
                    self.flashes += 1;
                    self.set(octo.x, octo.y, 0, false);
                }
            }
        }

        fn flash_octopus(&mut self) -> usize {
            // go over the cavern and check if there is an octopus with a value of 9 or higher. Make
            // this octopus flash and increase the neighbour value by one.

            let mut flashed: usize = 0;

            // let cloned = self.clone();
            for octo in self.octopus {

                if octo.energy > 9 && !octo.flash {
                    flashed += 1;
                    self.set(octo.x, octo.y, octo.energy + 1, true);

                    for neighbour in self.neighbours(&octo) {
                        self.set(neighbour.x, neighbour.y, neighbour.energy + 1, neighbour.flash)
                    }
                }
            }

            flashed
        }
    }



    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day11/d11.test.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let mut cavern = Cavern::new();

        let mut y = 0;
        for line in all_lines {
            let mut x = 0;
            for n in line.bytes() {
                let energy_int = n - b'0';
                cavern.set(x, y, energy_int, false);
                x +=1 ;
            }
            y += 1;
        }

        println!("Start:");
        println!("cavern:\n{}", cavern);

        for step in 1..=100 {
            cavern.step();
            println!("cavern: step {}\n{}\nflash count:{}\n", step, cavern, cavern.flashes);
        }
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

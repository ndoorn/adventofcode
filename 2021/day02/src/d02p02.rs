use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

pub fn main() {
    let lines = read_lines("/home/ndoorn/coding/personal/aoc/2021/day02/d02.input.txt").unwrap();

    let mut aim: i32 = 0;
    let mut depth: i32 = 0;
    let mut hor_pos: i32 = 0;

    let filtered_lines: Vec<String> = lines
        .filter(|item| item.is_ok())
        .map(|item| item.unwrap())
        .collect();

    for line in filtered_lines {
        if line.len() == 0 {
            continue;
        }

        let mut split = line.split(' ');

        let instruction = split.next().unwrap();
        let value = split.next().unwrap().parse::<i32>().unwrap();

        match instruction {
            "forward" => {
                hor_pos += value;
                depth += value * aim
            }
            "down" => aim += value,
            "up" => aim -= value,
            _ => unreachable!(),
        }
    }

    println!(
        "answer is {} . d {} , hp {}",
        depth * hor_pos,
        depth,
        hor_pos
    );
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

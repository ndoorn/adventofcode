use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

pub fn main() {
    let lines = read_lines("/home/ndoorn/coding/personal/aoc/2021/day02/d02.input.txt").unwrap();

    let mut depth: i32 = 0;
    let mut hor_pos: i32 = 0;

    lines
        .filter_map(|item| item.ok())
        .filter(|line| line.len() > 0)
        .map(|line| {
            let x = line.split_once(' ').unwrap();
            (x.0.to_owned(), x.1.parse::<i32>().unwrap())
        })
        .for_each(|v| {
            match v.0.as_str() {
                "forward" => hor_pos += v.1,
                "down" => depth += v.1,
                "up" => depth -= v.1,
                _ => unreachable!(),
            }
        });

    println!("answer is {} . d {} , hp {}", depth * hor_pos, depth, hor_pos);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

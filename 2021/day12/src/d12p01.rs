pub mod d12p1 {
    use std::borrow::BorrowMut;
    use std::collections::HashMap;
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::ops::Deref;
    use std::path::Path;

    #[derive(Debug, Clone)]
    struct Breadcrumbs {
        history: Vec<String>,
    }

    impl Breadcrumbs {
        fn new() -> Breadcrumbs {
            Breadcrumbs { history: vec![] }
        }
    }

    #[derive(Debug, Clone)]
    struct Cave {
        name: String,
        neighbours: Vec<String>,
    }

    impl Cave {
        fn new(name: String) -> Cave {
            Cave {
                name,
                neighbours: vec![],
            }
        }

        fn push_neighbour(&mut self, neighbour: &str) {
            self.neighbours.push(neighbour.to_owned());
        }

        fn is_big(&self) -> bool {
            return self.name == self.name.to_uppercase();
        }

        fn is_small(&self) -> bool {
            return self.name == self.name.to_lowercase();
        }
    }

    pub fn main() {
        let all_lines: Vec<String> =
            read_lines("/home/ndoorn/coding/personal/aoc/2021/day12/d12.input.txt")
                .unwrap()
                .map(|item| item.unwrap())
                .collect();

        let map = parse_input(all_lines);

        for entry in &map {
            let cave = entry.1;
            println!("Cave {} has {:?} as neighbours", cave.name, cave.neighbours)
        }

        let start = map.get("start").unwrap();

        println!("start is: {:?}", start);

        let mut found_routes: Vec<Breadcrumbs> = vec![];

        for neighbour in &start.neighbours {
            let mut breadcrumbs = Breadcrumbs::new();
            breadcrumbs.history.push("start".to_string());

            find_routes(&mut found_routes, breadcrumbs, &map, neighbour);
        }

        for x in &found_routes {
            println!("route: {:?}", x.history);
        }
        println!("Found {} routes", &found_routes.len());
    }

    fn find_routes(
        final_routes: &mut Vec<Breadcrumbs>,
        mut breadcrumbs: Breadcrumbs,
        map: &HashMap<String, Cave>,
        position: &String,
    ) {
        // check for neighbours, check if any available.
        // if any available spawn more workers
        // if none available and not at end, exit
        // if at end, exit

        if is_small(&position) && breadcrumbs.history.contains(position) {
            println!("Breadcrumbs contains position, {} -> {:?}", position, &breadcrumbs.history);
            return;
        }

        breadcrumbs.history.push(position.to_owned());

        println!("{:?} -> {}", &breadcrumbs.history, position);

        // Check if we are at the end location
        if position == "end" {
            println!("reached end, done: {:?}", &breadcrumbs);
            &final_routes.push(breadcrumbs);
            return;
        }

        let neighbours = &map.get(position).unwrap().neighbours;
        for x in neighbours {
            println!("Spawning new path for {} -> {}", position, x);
            let new_breadcrumbs = breadcrumbs.clone();

            find_routes(final_routes, new_breadcrumbs, map, x);
        }
    }

    fn is_small(input: &str) -> bool {
        return input == input.to_lowercase();
    }

    fn parse_input(input: Vec<String>) -> HashMap<String, Cave> {
        let mut set: HashMap<String, Cave> = HashMap::new();

        for line in input {
            let mut split = line.split('-');
            let left = split.next().unwrap();
            let right = split.next().unwrap();

            if !set.contains_key(left) {
                let cave = Cave::new(String::from(left));
                set.insert(left.to_owned(), cave);
            }

            if !set.contains_key(right) {
                let cave = Cave::new(String::from(right));
                set.insert(right.to_owned(), cave);
            }

            set.get_mut(left).unwrap().push_neighbour(right);
            set.get_mut(right).unwrap().push_neighbour(left);
        }

        return set;
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}

pub mod d7p1 {
    use std::fs::File;
    use std::io::{self, BufRead};
    use std::path::Path;

    pub fn main() {
        let all_lines: Vec<String> = read_lines("./d07.test.txt")
            .unwrap()
            .map(|item| item.unwrap())
            .collect();

        let input = all_lines.first().unwrap();
        let numbers = input
            .split(',')
            .map(|i| i.parse::<isize>().unwrap())
            .collect::<Vec<_>>();

        println!("input: {}", input);
        println!("numbers: {:?}", numbers);

        let max = numbers.iter().max().unwrap();
        println!("highest number is {}", max);

        let mut fuel_costs = vec![0; *max as usize + 1];
        println!("fuel_costs: {:?}", fuel_costs);

        for fuel_index in 0..(*max + 1) as usize {
            let mut fuel_cost = 0;

            for crab_position in &numbers {
                fuel_cost += (crab_position - fuel_index as isize).abs();
            }

            println!("pos {} costs {} fuel", fuel_index, fuel_cost);
            fuel_costs[fuel_index] = fuel_cost;
        }

        let loweset_fuel = fuel_costs.iter().min().unwrap();
        println!("lowest fuel cost is {}", loweset_fuel);
    }

    fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(filename)?;
        Ok(io::BufReader::new(file).lines())
    }
}
